const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product name is required"]
    },
    slug: {
        type: String,
        required: [true, "Slug is required"]
    },
    description: String,
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    discount: {
        type: Number,
        default: 0
    },
    discountedPrice: {
        type: Number,
        required: [true, "Price is required"]
    },
    stocks: {
        type: Number,
        default: 0
    },
    categoryId: String,
    createdOn: {
        type: Date,
        default: new Date()
    },
    isActive : {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    photoUrl: String
});

module.exports = mongoose.model("Product", productSchema);