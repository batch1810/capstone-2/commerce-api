const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product name is required"]
    },
    description: String,
    slug: {
        type: String,
        required: [true, "Slug is required"]
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    isActive: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model("Category", categorySchema);