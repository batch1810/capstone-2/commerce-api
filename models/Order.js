const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    orderIdNumber: {
        type: String,
        required: [true, "Order Id Number is required"]
    },
    userId: {
        type: String,
        required: [true, "User Id is required"]
    },
    totalAmount: {
        type: Number,
        required: [true, "Total amount is required"]
    },
    purchasedOn: {
        type: Date,
        default: null
    },
    status: {
        type: String,
        default: "In-Cart"
    },
    cartItemCount: Number,
    products: [
        {
            _id : false,
            productId: {
                type: String,
                required: [true, "Product Id is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is required"]
            },
            price: Number,
            subtotal: Number
        }
    ]
})

module.exports = mongoose.model("Order", orderSchema);