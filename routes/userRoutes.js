const express = require("express");
const router = express.Router();
const auth = require("../auth");


const userControllers = require("../controllers/userControllers");

// Router for checking if the email exists
router.post("/checkEmail", (req, res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Router for the user registration
router.post("/register",(req, res) =>{
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for the user login (with token creation)
router.post("/login", (req, res)=>{
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to set a user as Admin
router.put("/:userId/setAsAdmin",auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization); //contains the token 
	if(userData.isAdmin){
		userControllers.setAsAdmin(req.params.userId).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("Failed")
}
})

// Route for the retrieving the current user's details
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); //contains the token 

	// Provides the user's ID for the getProfile controller method
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

// Get customers
router.get("/customers", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin) {
		userControllers.getCustomers().then(resultFromController => res.send(resultFromController));
	}
	else {
		res.send([]);
	}
})

// Get administrators
router.get("/administrators", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin) {
		userControllers.getAdministrators().then(resultFromController => res.send(resultFromController));
	}
	else {
		res.send([]);
	}
})

module.exports = router;