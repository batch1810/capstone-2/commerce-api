const express = require("express");
const router = express.Router();
const auth = require("../auth");
const categoryController = require("../controllers/categoryController");

// Create category
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		categoryController.addCategory(req.body).then(resultFromController => res.send(resultFromController));
	}
	else {
		// 401 : Unauthorized HTTP response status code
		return res.send({ errorCode: 401 });
	}
});

// Retrieve categories
router.get("/", (req, res) => {
    categoryController.getCategories().then(resultFromController => res.send(resultFromController));
});

// Retrieve active categories only
router.get("/active", (req, res) => {
    categoryController.getActiveCategories().then(resultFromController => res.send(resultFromController));
});

// Update category
router.put("/:categoryId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		categoryController.updateCategory(req.params.categoryId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else {
        // 401 : Unauthorized HTTP response status code
		return res.send({ errorCode: 401 });
	}
});

// Update category status
router.patch("/:categoryId/change-status", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		categoryController.updateStatus(req.params.categoryId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else {
        // 401 : Unauthorized HTTP response status code
		return res.send({ errorCode: 401 });
	}
});

// Archive category
router.patch("/:categoryId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		categoryController.archiveCategory(req.params.categoryId).then(resultFromController => res.send(resultFromController));
	}
	else {
        // 401 : Unauthorized HTTP response status code
		return res.send({ errorCode: 401 });
	}
});

module.exports = router;