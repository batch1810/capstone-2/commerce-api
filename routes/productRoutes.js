const express = require("express");
const router  = express.Router();
const auth    = require("../auth");
const productControllers = require("../controllers/productControllers")

// Adding a product
router.post("/", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
	else {
		return res.send(false);
	}

})

// Retrieve products
router.get("/", (req, res) => {
	productControllers.getProducts().then(resultFromController =>res.send(resultFromController));
})


// Retrieving all active Products
router.get("/active", (req, res) => {
	productControllers.getAllActiveProduct().then(resultFromController =>res.send(resultFromController));
})

// Retrieve discounted products
router.get("/discounted", (req, res) => {
	productControllers.getDiscountedProducts().then(resultFromController =>res.send(resultFromController));
})

// Retrieve products by category
router.get("/:slug/category", (req, res) => {
	productControllers.getProductsByCategorySlug(req.params.slug).then(resultFromController => res.send(resultFromController));
})

// Retrieve product by product slug
router.get("/:slug/product", (req, res) => {
	productControllers.getProductBySlug(req.params.slug).then(resultFromController => res.send(resultFromController));
})

// Retrieve specific product
router.get("/:productId", (req, res) => {
	productControllers.getSpecificProduct(req.params.productId).then(resultFromController =>res.send(resultFromController));
})

// Update product info(admin only)
router.put("/:productId", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	
	if(userData.isAdmin){
		productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		return res.send(false);
	}
})

// Update product status
router.patch("/:productId/change-status", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		productControllers.updateStatus(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
	}
	else {
		return res.send(false);
	}
})

// Archive a product
router.patch("/:productId/archive", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	
	if (userData.isAdmin) {
		productControllers.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
	}
	else {
		return res.send(false);
	}

})

// ADD-ON
// Search product
router.get("/search/:keyword", (req, res) => {
	productControllers.search(req.params.keyword.trim()).then(resultFromController => res.send(resultFromController))
})

// Get product category menu
router.get("/categories/menu", (req, res) => {
	productControllers.getCategoryMenu().then(resultFromController => res.send(resultFromController))
})

// Get products by category
router.get("/categories/:category", (req, res) => {
	productControllers.getProductsByCategory(req.params.category.trim()).then(resultFromController => res.send(resultFromController))
})

module.exports = router;
