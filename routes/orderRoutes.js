const express = require("express");
const router  = express.Router();
const auth    = require("../auth");
const orderControllers = require("../controllers/orderControllers");

// Create Order
router.post("/checkout", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin === false) {
		orderControllers.createOrder(userData.id, req.body).then(resultFromController => res.send(resultFromController));
	}
	else {
		return res.send(false);
	}
});

// Get User Orders
router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin === false) {
		orderControllers.getUserOrders(userData.id).then(resultFromController => res.send(resultFromController));
	}
	else {
		return res.send(false);
	}
});

// Get All Orders
router.get("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		orderControllers.getAllOrders().then(resultFromController => res.send(resultFromController));
	}
	else {
		return res.send(false);
	}
});

// Add to cart
router.post("/addToCart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin === false) {
		orderControllers.addToCart(userData.id, req.body).then(resultFromController => res.send(resultFromController));
	}
	else {
		// Error Code: 0 - Admin is not allowed to create cart order
		return res.send({ errorCode: 0 });
	}
});

// Get in-cart order
router.get("/inCartOrder", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin === false) {
		orderControllers.getInCartOrder(userData.id).then(resultFromController => res.send(resultFromController));
	}
	else {
		return res.send(false);
	}
});

// Checkout cart
router.put("/:orderId/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin === false) {
		orderControllers.checkoutOrder(req.params.orderId).then(resultFromController => res.send(resultFromController));
	}
	else {
		return res.send(false);
	}
});

// Remove item from cart
router.put("/:productId/removeFromCart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin === false) {
		orderControllers.removeItemFromCart(userData.id, req.params.productId).then(resultFromController => res.send(resultFromController));
	}
	else {
		return res.send(false);
	}
});

module.exports = router;