const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// result is equal to an empty array ([]);
		// if result is greater than 0, user is found/already exists.
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else{ 
			return false;
		}
	});
}

// User Registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body.
	2. Make sure that the password is encrypted.
	3. Save the new User to the database.

*/

module.exports.registerUser = async (reqBody) =>{
	const emailExists = await User.find({email: reqBody.email}).then(result => {
		// result is equal to an empty array ([]);
		// if result is greater than 0, user is found/already exists.
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else{ 
			return false;
		}
	});

	if (emailExists) return false;

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		// Syntax: bcrypt.hashSync(dataToBeEncrypted, salt)
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) =>{
		if(error) {
			return false;
		}
		else {
			return true;
		}
	})
}


// User login
/*
	Steps:
	1. Check the database if the user's email is registered.
	2. Compare the password provided in the login form with the password stored in the database.
	3. Generate and return a JSON Web Token if the user is successfully login and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{

		// User does not exist
		if(result == null){
			return false;
		}
		// User exists
		else{
			// Syntax: bcrypt.compareSync(data, encrypted);
			// reqBody.password & result.password
			// bcrypt.compareSync() return Boolean
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the passwords match the result.
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			// Password do not match
			else{
				return false;
			}
		}

	})
}

// Admin only can set a user to another admin
module.exports.setAsAdmin = (userId) =>{

	const updateUser = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(userId, updateUser).then((UpdatedtoAdmin, error) =>{
		if (error) {
			return false;
		}
		else {
			return UpdatedtoAdmin //true;
		}
	})
	
}

module.exports.getProfile = async (data) => {
	let user = await User.findById(data.userId);
	user.password = "";
	const orders = await Order.find({ userId: data.userId, status: "In-Cart" });
	return { user, cartItemCount: orders.length > 0 ? orders[0].cartItemCount : 0 };
}

// Get customers
module.exports.getCustomers = () => {
	return User.find({ isAdmin: false }).then(result => result);
}

// Get administrators
module.exports.getAdministrators = () => {
	return User.find({ isAdmin: true }).then(result => result);
}