const Order = require("../models/Order");
const Product = require("../models/Product");
const productControllers = require("./productControllers");
const mongoose = require("mongoose");

// Create Order
module.exports.createOrder = async (userId, order) => {
	// Get order products price
	const productPrices = await Product.find({ 
		_id: { 
			$in: order.products.map(p => mongoose.Types.ObjectId(p.productId))
		}
	}, {
		price: 1
	});

	// Set product price and subtotal
	order.products = order.products.map(product => {
		// get product price
		const price = productPrices.find(p => p._id.toString() === product.productId).price;
		// set product order price
		product.price = price;
		// set product order subtotal
		product.subtotal = price * product.quantity;
		return product;
	});

	// Set order total amount
	const totalAmount = order.products.map(p => p.subtotal).reduce((prevVal, currVal) => prevVal + currVal);

	//Instantiate new order 
	const newOrder = new Order({
		userId: userId,
		totalAmount: totalAmount,
		products: order.products
	});

	return newOrder.save().then((order, error) =>{
		if(error) {
			return false;
		}
		else {
			// Deduct stocks of the products
			order.products.forEach(product => {
				productControllers.deductStocks(product.productId, product.quantity).then(success => {
					if (success === false) {
						console.log(`Product ID: (${product.productId}). Error: Fail to deduct stocks after order checkout.`);
					}
				});
			});
			
			return order;
			// return true;
		}
	});
}

// Get User Orders (Customer)
module.exports.getUserOrders = (userId) => {	
	return Order.find({ userId: userId, status: { $ne: "In-Cart" } }).then(result => result);
}

// Get All Orders (Admin)
module.exports.getAllOrders = () => {
	return Order.find({ status: { $ne: "In-Cart" } }).then(result => result);
}

// Add to Cart
module.exports.addToCart = async (userId, reqBody) => {
	const { productId, quantity } = reqBody;
	const productToAdd = await Product.findById(productId);
	const { discountedPrice, stocks } = productToAdd;

	const newCartItem = {
		productId: productId,
		quantity: quantity,
		price: discountedPrice,
		subtotal: discountedPrice * quantity
	};

	const orders = await Order.find({ userId: userId, status: "In-Cart" });
	if (orders.length > 0) {
		const { products, totalAmount, cartItemCount } = orders[0];
		let updatedOrder = {
			totalAmount: totalAmount + newCartItem.subtotal,
			cartItemCount: cartItemCount + newCartItem.quantity
		};
		if (products.findIndex(p => p.productId === newCartItem.productId) > -1) {
			const inCartProduct = products.find(p => p.productId === newCartItem.productId);
			if ((newCartItem.quantity + inCartProduct.quantity) > stocks) {
				// Error Code: 1 - Product stock is not enough
				return { errorCode: 1 }
			}
			const updatedCartItem = {
				productId: newCartItem.productId,
				quantity: newCartItem.quantity + inCartProduct.quantity,
				price: newCartItem.price,
				subtotal: newCartItem.price * (newCartItem.quantity + inCartProduct.quantity)
			}
			updatedOrder.products = products.map(p => p.productId === newCartItem.productId ? updatedCartItem : p);			
		}
		else {
			updatedOrder.products = [...products, newCartItem];
		}
		
		return Order.findByIdAndUpdate(orders[0]._id, updatedOrder).then((order, err) =>{
			if (err) {
				// Error Code: 2 - Unable to update cart
				return { errorCode: 2 }
			}
			else {
				return true;
			}
		})
	}
	else {
		const currentDate = new Date();
		const year = currentDate.getFullYear()
		const allOrdersCount = await Order.count({});
		const order = new Order({
			orderIdNumber: `${year}${allOrdersCount + 1}`,
			userId: userId,
			totalAmount: newCartItem.subtotal,
			cartItemCount: quantity,
			products: [newCartItem]
		});
		
		return order.save().then((order, error) => {
			if (error) {
				// Error Code: 3 - Unable to create cart
				return { errorCode: 3 };
			}
			else {
				return order;
			}
		})
	}
}

// Get in-cart order
module.exports.getInCartOrder = async (userId) => {
	const orders = await Order.find({ userId: userId, status: "In-Cart" });
	if (orders.length > 0) {
		const cart = orders[0];
		const products = await Product.find({ _id: { $in: cart.products.map(p => mongoose.Types.ObjectId(p.productId)) } })
		return { cart, products }
	}
	return { cart: null, products: [] };
}

// Checkout order
module.exports.checkoutOrder = async (orderId) => {
	const cartOrder = {
		purchasedOn: new Date(),
		status: "Pending"
	};
	const updated = await Order.findByIdAndUpdate(orderId, cartOrder);
	if (updated) {
		const order = await Order.findById(orderId);
		order.products.forEach(async (op) => {
			const product = await Product.findById(op.productId);
			const updatedStock = {
				stocks: product.stocks - op.quantity
			};
			Product.findByIdAndUpdate(op.productId, updatedStock).then((p, err) => true);
		});
		return true;
	}

	return false;
}

// Delete item from cart
module.exports.removeItemFromCart = async (userId, productId) => {
	const orders = await Order.find({ userId: userId, status: "In-Cart" });
	const { totalAmount, cartItemCount, products } = orders[0];
	const productToRemove = products.find(p => p.productId === productId);
	const updatedOrder = {
		totalAmount: totalAmount - productToRemove.subtotal,
		cartItemCount: cartItemCount - productToRemove.quantity,
		products: products.filter(p => p.productId !== productId)
	};
	return Order.findByIdAndUpdate(orders[0]._id, updatedOrder).then((order, err) =>{
		if (err) {
			return false;
		}
		else {
			return true;
		}
	})
}