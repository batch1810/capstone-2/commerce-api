const Product = require("../models/Product");
const categoryController = require("./categoryController");

// Add products
module.exports.addProduct = async (reqBody) => {
	const products = await Product.find({ isDeleted: false, slug: reqBody.name.trim().replace(/\s+/g, '-').toLowerCase() });

	let slug = reqBody.name.trim().replace(/\s+/g, '-').toLowerCase();

	if (products.length > 0) {
		slug += `-${products.length}`;
	}

	const discountedPrice = reqBody.discount > 0 ? (reqBody.price - (reqBody.price * (reqBody.discount / 100))) : reqBody.price;

	const newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		slug: slug,
		price: reqBody.price,
		discount: reqBody.discount,
		discountedPrice: discountedPrice,
		stocks: reqBody.stocks,
		categoryId: reqBody.categoryId,
		photoUrl: reqBody.photoUrl,
		isActive: reqBody.isActive
	});

	return newProduct.save().then((product, error) => {
		if(error) {
			return false;
		}
		else {
			return product;
		}
	});
}

// Retrieve all products
// Retrieve all active products
module.exports.getProducts = () => {
	return Product.find({isDeleted: false}).then(result => result)
}

// Retrieve all active products
module.exports.getAllActiveProduct = () => {
	return Product.find({isActive: true, isDeleted: false}).then(result => result)
}

// Retrieve discounted products
module.exports.getDiscountedProducts = () => {
	return Product.find({
		isActive: true, 
		isDeleted: false, 
		discount: { $gt: 0 }
	}).then(result => result)
}

// Retrieve products by category
module.exports.getProductsByCategorySlug = async (categorySlug) => {
	const categories = await categoryController.getCategory(categorySlug);
	if (categories.length > 0) {
		const products = await Product.find({
			isActive: true, 
			isDeleted: false, 
			categoryId: categories[0]._id.toString()
		});

		return { category: categories[0], products }
	}

	return { category: { name: "" }, products: [] }
}

// Retrive product by product slug
module.exports.getProductBySlug = async (productSlug) => {
	const product = await Product.find({
		isActive: true, 
		isDeleted: false, 
		slug: productSlug
	});

	if (product.length === 0) return false;

	const category = await categoryController.getCategoryById(product[0].categoryId);

	return { category, product }
}

// Retrieve specific product
module.exports.getSpecificProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}


// Update product

module.exports.updateProduct = async (productId, reqBody) => {
	const products = await Product.find({ isDeleted: false, slug: reqBody.name.trim().replace(/\s+/g, '-').toLowerCase() });

	let slug = reqBody.name.trim().replace(/\s+/g, '-').toLowerCase();

	if (products.length > 0) {
		slug += `-${products.length}`;
	}

	const discountedPrice = reqBody.discount > 0 ? (reqBody.price - (reqBody.price * (reqBody.discount / 100))) : reqBody.price;

	const updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		slug: slug,
		price: reqBody.price,
		discount: reqBody.discount,
		discountedPrice: discountedPrice,
		stocks: reqBody.stocks,
		categoryId: reqBody.categoryId,
		photoUrl: reqBody.photoUrl,
		isActive: reqBody.isActive
	};

	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, err) =>{
			if(err) {
				return false;
			}
			else {
				return true;
			}
		})
}

// Update status
module.exports.updateStatus = (productId, reqBody) => {
	const archivedProduct = {
		isActive: reqBody.isActive
	};

	return Product.findByIdAndUpdate(productId, archivedProduct).then((productUpdate, err)=>{
		if (err) {
			return false;
		}
		else {
			return true;
		}
	})
}

// archive a product
module.exports.archiveProduct = (productId) => {
	const archivedProduct = {
		isDeleted: true
	}
	return Product.findByIdAndUpdate(productId, archivedProduct).then((productUpdate, err)=>{
		if (err) {
			return false;
		}
		else {
			return true;
		}
	})
}

// deduct product stocks
module.exports.deductStocks = (productId, orderQty) => {
	return Product.findById(productId).then((product, error) =>{
		if(error) {
			return false;
		}
		else{
			// reassign product stocks with its stocks minus order quantity
			product.stocks -= orderQty;

			return product.save().then((updatedProd, updateErr) => {
				if(updateErr) {
					return false
				}
				else {
					return true;
				}
			})
		}
	})
}

// ADD-ON
// Search product
module.exports.search = (keyword) => {
	return Product.find({
		isActive: true,
		$or: [
			{ name: { $regex: keyword, $options: "$i" } }, 
			{ description: { $regex: keyword, $options: "$i" } },
			{ tags: { $regex: keyword, $options: "$i" } }
		]
	}).then(result => result);
}

// Get product category menu
module.exports.getCategoryMenu = async () => {
	// get categories from products
	const productCategories = await Product.find({ isActive: true }, { _id: 0, categories: 1 });
	// merge categories into one array
	const mergedCategories = [].concat.apply([], productCategories.map(product => product.categories));
	// remove duplicates
	const uniqueCategories = Array.from(new Set(mergedCategories));
	return uniqueCategories;
}

// Get products by category
module.exports.getProductsByCategory = (category) => {
	return Product.find({ isActive: true, categories: category }).then(result => result);
}

