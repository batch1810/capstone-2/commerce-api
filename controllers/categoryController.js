const Category = require("../models/Category");
const Product = require("../models/Product");

// Create category
module.exports.addCategory = async (reqBody) => {
    const categories = await Category.find({ 
        $and: [
            { slug: reqBody.name.trim().replace(/\s+/g, '-').toLowerCase() },
            { isDeleted: false }
        ]
     });

    if (categories.length > 0) return { errorCode: 2 };

    const newCategory = new Category({
		name: reqBody.name,
        description: reqBody.description,
		slug: reqBody.name.trim().replace(/\s+/g, '-').toLowerCase()
	});

	return newCategory.save().then((category, error) =>{
		if (error) {
			return { errorCode: 1 };
		}
		else {
			return category;
		}
	});

    // Error codes
    // 1 : Fail to add category
    // 2 : Category already exists
}

// Retrieve categories
module.exports.getCategories = () => {
    return Category.find({ isDeleted: false }).then(result => result);
}

// Retrieve active categories only
module.exports.getActiveCategories = () => {
    return Category.find({ isActive: true, isDeleted: false }).then(result => result);
}

// Retrieve category by slug
module.exports.getCategory = (categorySlug) => {
    return Category.find({ isActive: true, isDeleted: false, slug: categorySlug }).then(result => result);
}

// Retrieve category by id
module.exports.getCategoryById = (categoryId) => {
    return Category.find({ isActive: true, isDeleted: false, _id: categoryId}).then(result => result);
}

// Update category
module.exports.updateCategory = async (categoryId, reqBody) => {
    // Get current category
    const currentCategory = await Category.findById(categoryId);

    // Find categories where name is not equal to current name and equal to updated name
    let categories = [];
    if (reqBody.name.trim().toLowerCase() !== currentCategory.name.toLowerCase()) {
        categories = await Category.find({ 
            $and: [
                { name: { $ne:  currentCategory.name } }, 
                { slug: reqBody.name.trim().replace(/\s+/g, '-').toLowerCase() },
                { isDeleted: false }
            ]
        });
    }

    if (categories.length > 0) return { errorCode: 2 };

    const category = {
		name: reqBody.name,
		description: reqBody.description,
        slug: reqBody.name.trim().replace(/\s+/g, '-').toLowerCase()
	}

	return Category.findByIdAndUpdate(categoryId, category).then((category, err) => {
        if(err) {
            return { errorCode: 1};
        }
        else {
            return category;
        }
    });

    // Error codes
    // 1 : Fail to add category
    // 2 : Category already exists
}

// Update category status
module.exports.updateStatus = (categoryId, reqBody) => {
    const category = {
		isActive: reqBody.isActive
	};

	return Category.findByIdAndUpdate(categoryId, category).then((category, err) =>{
        if(err) {
            return false;
        }
        else {
            return true;
        }
    });
}

// Archive category
module.exports.archiveCategory = async (categoryId) => {
    const products = await Product.find({ categoryId: categoryId, isDeleted: false });

    if (products.length > 0) {
        // Error Code: 1 - Cannot delete a category with products
        return { errorCode: 1 }
    }

    const category = {
		isDeleted: true
	};

	return Category.findByIdAndUpdate(categoryId, category).then((category, err) =>{
        if(err) {
            return false;
        }
        else {
            return true;
        }
    });
}

